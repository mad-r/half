//
// Created by a404m on 10/3/23.
//

#ifndef HALF_GATE_H
#define HALF_GATE_H

#include <compiler/node/Node.h>
#include <compiler/class/Class.h>
#include <interpreter/wire/Wire.h>

namespace half {
    class ClassCompiler;
    class Gate {
    public:
        std::vector<Wire*> wires;
        size_t argSize = 0;
        std::vector<Gate> childGates;
        void (*runFunction)(Gate *gate) = nullptr;

        Gate(const Class *clazz,const std::vector<Wire*>& args,const ClassCompiler *classCompiler);

        Gate(const Gate &gate) = default;
        Gate(Gate &&gate) = default;

        ~Gate();

        Gate &operator=(const Gate &gate) = default;
        Gate &operator=(Gate &&gate) = default;
    private:
        std::vector<Wire*> getWires(const Node &compiledRunNode);
    public:
        void run();

    private:
        static void runNotNative(Gate *gate);
        static void runNAnd(Gate *gate);
        static void runTBuff(Gate *gate);

    public:
        void setWiresToNext();
    };
} // half

#endif //HALF_GATE_H

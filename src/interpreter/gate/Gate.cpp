//
// Created by a404m on 10/3/23.
//

#include "Gate.h"
#include <compiler/class-compiler/ClassCompiler.h>

namespace half {
    Gate::Gate(const Class *clazz, const std::vector<Wire *> &args, const ClassCompiler *classCompiler) :
            wires(clazz->inouts.size() + clazz->wires.size()),
            argSize(clazz->inouts.size()) {
        if (!clazz->isNative) {
            if (args.size() != argSize)
                throw std::logic_error("too few or too many args");
            for (size_t i = 0; i < args.size(); i++) {
                wires[i] = args[i];
            }
            for (size_t i = args.size(); i < wires.size(); i++) {
                wires[i] = new Wire();
            }

            for (const auto &node: clazz->compiledRun) {
                const auto &className = node.operands.front().str;
                Class *const childClass = classCompiler->getClass(className);

                childGates.emplace_back(
                        childClass,
                        getWires(node),
                        classCompiler
                );
            }
            runFunction = runNotNative;
        } else {
            argSize = args.size();
            wires = args;
            if (clazz->name == "nand") {
                runFunction = runNAnd;
            } else if (clazz->name == "tbuff") {
                runFunction = runTBuff;
            } else {
                throw std::runtime_error("unknown native class '" + clazz->name + "'");
            }
        }
    }

    Gate::~Gate() {
        for (size_t i = argSize; i < wires.size(); i++) {
            delete wires[i];
        }
    }

    std::vector<Wire *> Gate::getWires(const Node &compiledRunNode) {
        std::vector<Wire *> result;
        const auto &operands = compiledRunNode.operands;
        for (size_t i = 1; i < operands.size(); i++) {
            auto &node = operands[i];
            Wire *wire;
            switch (node.token) {
                case Node::IDENTIFIER:
                    wire = wires[std::stoll(node.str)];
                    break;
                case Node::NONE:
                case Node::KW_CLASS:
                case Node::KW_INOUT:
                case Node::KW_WIRE:
                case Node::KW_RUN:
                case Node::KW_GATE:
                case Node::OP_OPEN_CURLY_BRACKET:
                case Node::OP_CLOSE_CURLY_BRACKET:
                case Node::OP_COMMA:
                case Node::OP_SEMICOLON:
                    throw std::runtime_error("unexpected token");
                case Node::V_L:
                    wire = wires.emplace_back(new Wire(true, Wire::State::L));
                    break;
                case Node::V_H:
                    wire = wires.emplace_back(new Wire(true, Wire::State::H));
                    break;
                case Node::V_Z:
                    wire = wires.emplace_back(new Wire(true, Wire::State::Z));
                    break;
                case Node::V_X:
                    wire = wires.emplace_back(new Wire(true, Wire::State::X));
                    break;
                default:
                    throw std::runtime_error("undefined token");
            }
            result.push_back(wire);
        }
        return result;
    }

    void Gate::run() {
        this->runFunction(this);
    }

    void Gate::runNotNative(Gate *gate) {
        for(auto &g : gate->childGates){
            g.run();
        }
    }

    void Gate::runNAnd(Gate *gate) {
        auto &wires = gate->wires;
        auto s1 = wires[1]->current;
        auto s2 = wires[2]->current;
        Wire::State result;

        if (s1 == Wire::State::L || s2 == Wire::State::L)
            result = Wire::State::H;
        else if (s1 == Wire::State::H && s2 == Wire::State::H)
            result = Wire::State::L;
        else
            result = Wire::State::X;

        wires[0]->setNext(result);
    }

    void Gate::runTBuff(Gate *gate) {
        auto &wires = gate->wires;
        auto s1 = wires[1]->current;
        auto s2 = wires[2]->current;
        Wire::State result;
        switch (s2) {
            case Wire::State::H:
                result = s1;
                break;
            case Wire::State::L:
            case Wire::State::SHORTED:
                result = Wire::State::Z;
                break;
            case Wire::State::NONE:
                throw std::runtime_error("unknown wire state");
            default:
                result = Wire::State::X;
                break;
        }
        wires[0]->setNext(result);
    }

    void Gate::setWiresToNext() {
        for (auto it = wires.begin()+static_cast<long>(argSize), end = wires.end(); it < end; ++it) {
            (*it)->setToNext();
        }
        for (auto &child: childGates) {
            child.setWiresToNext();
        }
    }
} // half
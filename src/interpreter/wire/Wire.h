//
// Created by a404m on 10/3/23.
//

#ifndef HALF_WIRE_H
#define HALF_WIRE_H

#include <stdexcept>

namespace half {
    class Wire {
    public:
        enum class State{
            NONE,
            L,
            H,
            Z,
            X,
            SHORTED
        };
        State current = State::NONE;
        State next = State::NONE;
        const bool isConst = false;

        explicit Wire(bool _isConst = false,State state = State::NONE);

        void setToNext();
        void setNext(State state);
    };
} // half

#endif //HALF_WIRE_H

//
// Created by a404m on 10/3/23.
//

#include "Wire.h"

namespace half {
    Wire::Wire(bool _isConst,State state) : current(state), isConst(_isConst) {
        //empty
    }

    void Wire::setToNext() {
        if(isConst)
            return;
        if(next != State::NONE) {
            current = next;
            next = State::NONE;
        }
    }

    void Wire::setNext(Wire::State state) {
        if(isConst)
            return;
        switch (state) {
            case State::L:
                switch (next) {
                    case State::NONE:
                    case State::Z:
                        next = state;
                        break;
                    case State::H:
                        next = State::SHORTED;
                    case State::L:
                    case State::X:
                    case State::SHORTED:
                        break;
                    default:
                        throw std::runtime_error("unknown state");
                }
                break;
            case State::H:
                switch (next) {
                    case State::NONE:
                    case State::Z:
                        next = state;
                        break;
                    case State::L:
                        next = State::SHORTED;
                        break;
                    case State::H:
                    case State::X:
                    case State::SHORTED:
                        break;
                    default:
                        throw std::runtime_error("unknown state");
                }
                break;
            case State::Z:
                switch (next) {
                    case State::NONE:
                        next = state;
                        break;
                    case State::L:
                    case State::H:
                    case State::Z:
                    case State::X:
                    case State::SHORTED:
                        break;
                    default:
                        throw std::runtime_error("unknown state");
                }
                break;
            case State::X:
                switch (next) {
                    case State::NONE:
                        next = state;
                        break;
                    case State::L:
                    case State::H:
                    case State::X:
                        throw std::logic_error("shorted");
                    case State::Z:
                        next = state;
                        break;
                    default:
                        throw std::runtime_error("unknown state");
                }
                break;
            case State::SHORTED:
                throw std::runtime_error("assigning shorted");
            default:
                throw std::runtime_error("unknown state");
        }
    }
} // half
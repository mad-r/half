#include <iostream>
#include <chrono>
#include <compiler/gate-tree-maker/GateTreeMaker.h>

long process(std::string_view str, int frames);

long fastProcess(std::string_view str, int frames);

int main() {
    constexpr int size = 300;
    constexpr int frames = 10;
    long sum = 0;
    for (int i = 0; i < size; i++) {
        sum += process(
                R"(
class Main{
    inout a,b,c,d,e,f;
    wire a1,b1,c1,d1,e1,f1;
    run{
        gate And a1,a,a;
        gate And b1,b,b;
        gate And c1,c,c;
        gate And d1,d,d;
        gate And e1,e,e;
        gate And f1,f,f;
    }
}
class And{
    inout a,b,c;
    wire d;
    run{
        gate nand d,b,c;
        gate Not a,d;
    }
}
class Not{
    inout a,b;
    run{
        gate nand a,b,b;
    }
}
)", frames);
        std::cout << "===============================" << std::endl;
    }
    std::cout << ((double) sum) / size << "ns";

    return 0;
}

long process(std::string_view str, int frames) {
    auto start = std::chrono::high_resolution_clock::now();
    auto lexer = half::Lexer(str);
    auto end = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
    std::cout << "Lexer: " << duration << std::endl;

    start = std::chrono::high_resolution_clock::now();
    auto parser = half::Parser(std::move(lexer));
    end = std::chrono::high_resolution_clock::now();
    duration += std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
    std::cout << "Parser: " << duration << std::endl;

    start = std::chrono::high_resolution_clock::now();
    auto treeCleaner = half::TreeCleaner(std::move(parser));
    end = std::chrono::high_resolution_clock::now();
    duration += std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
    std::cout << "TreeCleaner: " << duration << std::endl;

    start = std::chrono::high_resolution_clock::now();
    auto classCompiler = half::ClassCompiler(std::move(treeCleaner));
    end = std::chrono::high_resolution_clock::now();
    duration += std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
    std::cout << "ClassCompiler: " << duration << std::endl;

    start = std::chrono::high_resolution_clock::now();
    auto gateTreeMaker = half::GateTreeMaker(std::move(classCompiler));
    end = std::chrono::high_resolution_clock::now();
    duration += std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
    std::cout << "GateTreeMaker: " << duration << std::endl;

    start = std::chrono::high_resolution_clock::now();
    gateTreeMaker.setInouts({
                                    half::Wire::State::L,
                                    half::Wire::State::H,
                                    half::Wire::State::L,
                                    half::Wire::State::H,
                                    half::Wire::State::L,
                                    half::Wire::State::H,
                            });
    for (int i = 0; i < frames; i++) {
        gateTreeMaker.run();
    }
    end = std::chrono::high_resolution_clock::now();
    auto runDuration = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
    duration += runDuration;
    std::cout << "run: " << duration << "\t\t" << runDuration << "\t\tfps average time: " << runDuration / frames << std::endl;

    return duration.count();
}

long fastProcess(std::string_view str, int frames) {
    auto start = std::chrono::high_resolution_clock::now();
    auto gateTreeMaker = half::GateTreeMaker(str);
    auto end = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
    std::cout << "Compile: " << duration << std::endl;

    start = std::chrono::high_resolution_clock::now();
    gateTreeMaker.setInouts({
                                    half::Wire::State::L,
                                    half::Wire::State::H,
                                    half::Wire::State::L,
                                    half::Wire::State::H,
                                    half::Wire::State::L,
                                    half::Wire::State::H,
                            });
    for (int i = 0; i < frames; i++) {
        gateTreeMaker.run();
    }
    end = std::chrono::high_resolution_clock::now();
    auto runDuration = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
    duration += runDuration;
    std::cout << "run: " << duration << "\t\t" << runDuration << "\t\tfps average time: " << runDuration / frames << std::endl;

    return duration.count();
}

//
// Created by a404m on 10/2/23.
//

#ifndef HALF_TREECLEANER_H
#define HALF_TREECLEANER_H

#include <compiler/parser/Parser.h>

namespace half {
    class TreeCleaner {
    public:
        Parser parser;

        explicit TreeCleaner(Parser _parser);
        explicit TreeCleaner(std::string_view code);

    private:
        static void cleanNode(Node &node);

        static void cleanClassNode(Node &node);
        static void cleanDefinitionNode(Node &node);
        static void cleanGateNode(Node &node);
        static void cleanRunNode(Node &node);
        static void cleanCodeBlock(Node &node);
        static void cleanSemicolon(Node &node);

        static std::vector<Node> getNodesFromComma(Node &node);
    };
} // half

#endif //HALF_TREECLEANER_H

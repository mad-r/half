//
// Created by a404m on 10/2/23.
//

#include "TreeCleaner.h"

namespace half {
    TreeCleaner::TreeCleaner(Parser _parser) : parser(std::move(_parser)) {
        for(auto &node : parser.lexer.lexed){
            cleanClassNode(node);
        }
    }

    TreeCleaner::TreeCleaner(std::string_view code) : parser(code) {
        for(auto &node : parser.lexer.lexed){
            cleanClassNode(node);
        }
    }

    void TreeCleaner::cleanNode(Node &node) {
        switch (node.token) {
            case Node::NONE:
                throw std::runtime_error("Node::NONE is not allowed here");
            case Node::IDENTIFIER:
                throw std::logic_error("unexpected token");
            case Node::KW_CLASS:
                cleanClassNode(node);
                return;
            //case Node::KW_IN:
            case Node::KW_INOUT:
            //case Node::KW_OUT:
            case Node::KW_WIRE:
                cleanDefinitionNode(node);
                return;
            case Node::KW_RUN:
                cleanRunNode(node);
                return;
            case Node::KW_GATE:
                cleanGateNode(node);
                return;
            case Node::V_L:
            case Node::V_H:
            case Node::V_Z:
            case Node::V_X:
            case Node::OP_OPEN_CURLY_BRACKET:
            case Node::OP_CLOSE_CURLY_BRACKET:
            case Node::OP_COMMA:
                throw std::logic_error("unexpected token");
            case Node::OP_SEMICOLON:
                cleanSemicolon(node);
                return;
        }
    }

    void TreeCleaner::cleanClassNode(Node &node) {
        for(auto &n : node.operands.back().operands){
            cleanNode(n);
        }
    }

    void TreeCleaner::cleanDefinitionNode(Node &node) {
        if(node.operands.size() == 1){
            if(node.operands.front().token == Node::IDENTIFIER) {
                return;
            }else if(node.operands.front().token == Node::OP_COMMA){
                node.operands = getNodesFromComma(node.operands.front());//todo check node tokens
            }else{
                throw std::logic_error("defining io needs a list of identifier");
            }
        }
    }

    void TreeCleaner::cleanGateNode(Node &node) {
        auto &front = node.operands.front();
        auto &back = node.operands.back();
        if(front.token != Node::IDENTIFIER){
            throw std::logic_error("gate needs an identifier");
        }
        if(back.token == Node::IDENTIFIER){
            return;
        }else if(back.token == Node::OP_COMMA){
            auto ids = getNodesFromComma(back);
            node.operands.erase(node.operands.begin()+1);
            node.operands.insert(node.operands.end(),ids.begin(),ids.end());
        }else{
            throw std::logic_error("gate needs a list of identifiers");
        }
    }

    void TreeCleaner::cleanRunNode(Node &node) {
        if(node.operands.front().token == Node::OP_CLOSE_CURLY_BRACKET){
            cleanCodeBlock(node.operands.front());
            node.operands = std::move(node.operands.front().operands);
        }else{
            throw std::logic_error("run needs a block of code");
        }
    }

    void TreeCleaner::cleanCodeBlock(Node &node) {
        for(auto &n : node.operands){
            cleanNode(n);
        }
    }

    void TreeCleaner::cleanSemicolon(Node &node) {
        if(node.operands.empty()){
            return;
        }else if(node.operands.size() == 1){
            cleanNode(node.operands.front());
            node = node.operands.front();
        }else{
            throw std::logic_error("semicolon is needed");
        }
    }

    std::vector<Node> TreeCleaner::getNodesFromComma(Node &node) {
        std::vector<Node> result;
        auto &front = node.operands.front();
        auto &back = node.operands.back();
        if(front.token == Node::OP_COMMA){
            auto temp = getNodesFromComma(front);
            result.insert(result.end(),std::make_move_iterator(temp.begin()),std::make_move_iterator(temp.end()));
        }else{
            result.emplace_back(std::move(front));
        }
        if(back.token == Node::OP_COMMA){
            auto temp = getNodesFromComma(back);
            result.insert(result.end(),std::make_move_iterator(temp.begin()),std::make_move_iterator(temp.end()));
        }else{
            result.emplace_back(std::move(back));
        }
        return result;
    }
} // half
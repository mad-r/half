//
// Created by a404m on 10/3/23.
//

#include "Class.h"
#include <algorithm>

namespace half {
    Class::Class(Node node) : name(std::move(node.operands.front().str)){
        Node *inoutNode = nullptr, *wireNode = nullptr, *runNode = nullptr;
        for(auto &n : node.operands.back().operands){
            switch (n.token) {
                case Node::KW_INOUT:
                    if(inoutNode != nullptr)
                        throw std::logic_error("duplicated inout");
                    else
                        inoutNode = &n;
                    break;
                case Node::KW_WIRE:
                    if(wireNode != nullptr)
                        throw std::logic_error("duplicated wire");
                    else
                        wireNode = &n;
                    break;
                case Node::KW_RUN:
                    if(runNode != nullptr)
                        throw std::logic_error("duplicated run");
                    else
                        runNode = &n;
                    break;
                default:
                    throw std::logic_error("unexpected token");
            }
        }

        if(inoutNode != nullptr){
            for(auto &n : inoutNode->operands){
                if(n.token != Node::IDENTIFIER)
                    throw std::logic_error("inout can only have a list of ids");
                inouts.emplace_back(std::move(n.str));
            }
        }
        if(wireNode != nullptr){
            for(auto &n : wireNode->operands){
                if(n.token != Node::IDENTIFIER)
                    throw std::logic_error("wire can only have a list of ids");
                wires.emplace_back(std::move(n.str));
            }
        }
        if(runNode != nullptr){
            run = std::move(runNode->operands);
        }
    }

    Class::Class(std::string _name) : name(std::move(_name)), isNative(true) {

    }

    int Class::wireNameToInt(std::string_view wireName) {
        auto it = std::find(inouts.begin(), inouts.end(), wireName);
        if(it != inouts.end())
            return it-inouts.begin();

        it = std::find(wires.begin(), wires.end(), wireName);

        if(it != wires.end())
            return (it-wires.begin())+inouts.size();

        return -1;
    }
} // half
//
// Created by a404m on 10/3/23.
//

#ifndef HALF_CLASS_H
#define HALF_CLASS_H

#include <compiler/node/Node.h>

namespace half {
    class Class {
        friend class ClassCompiler;
    public:
        std::string name;
        std::vector<std::string> inouts;
        std::vector<std::string> wires;
        std::vector<Node> run;
        std::vector<Node> compiledRun;
        bool isNative = false;

        explicit Class(Node node);
        explicit Class(std::string _name);

        int wireNameToInt(std::string_view wireName);
    };
} // half

#endif //HALF_CLASS_H

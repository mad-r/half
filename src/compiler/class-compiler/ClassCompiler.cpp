//
// Created by a404m on 10/3/23.
//

#include "ClassCompiler.h"
#include <algorithm>

namespace half {
    ClassCompiler::ClassCompiler(TreeCleaner _treeCleaner) : treeCleaner(std::move(_treeCleaner)){
        for(auto &node : treeCleaner.parser.lexer.lexed){
            classes.emplace_back(
                new Class(std::move(node))
            );
        }

        compileRunNodes();
    }

    ClassCompiler::ClassCompiler(std::string_view code) : treeCleaner(code) {
        for(auto &node : treeCleaner.parser.lexer.lexed){
            classes.emplace_back(
                    new Class(std::move(node))
            );
        }

        compileRunNodes();
    }

    ClassCompiler::~ClassCompiler() {
        for(auto clazz : classes){
            delete clazz;
        }
    }

    Class *ClassCompiler::getClass(std::string_view className) const{
        const auto end = classes.end();
        const auto it = std::find_if(classes.begin(), end, [className](Class *const clazz){
            return clazz->name == className;
        });
        if(it == end)
            return nullptr;
        else
            return *it;
    }

    void ClassCompiler::compileRunNodes() {
        for(auto clazz : classes){
            auto &run = clazz->run;
            auto &compiledRun = clazz->compiledRun;
            compiledRun.clear();
            for(auto &gateNode : run){
                compiledRun.push_back(gateNode);
                for(size_t i = 1;i < compiledRun.back().operands.size();i++){
                    auto &node = compiledRun.back().operands[i];
                    switch (node.token) {
                        case Node::NONE:
                        case Node::KW_CLASS:
                        case Node::KW_INOUT:
                        case Node::KW_WIRE:
                        case Node::KW_RUN:
                        case Node::KW_GATE:
                        case Node::OP_OPEN_CURLY_BRACKET:
                        case Node::OP_CLOSE_CURLY_BRACKET:
                        case Node::OP_COMMA:
                        case Node::OP_SEMICOLON:
                            throw std::runtime_error("unexpected token");
                        case Node::IDENTIFIER: {
                            auto &wireName = node.str;
                            auto index = clazz->wireNameToInt(wireName);
                            if (index == -1)
                                throw std::logic_error("no wire found by name '" + wireName + "'");
                            wireName = std::to_string(index);
                        }
                            break;
                        case Node::V_L:
                        case Node::V_H:
                        case Node::V_Z:
                        case Node::V_X:
                            break;
                        default:
                            throw std::runtime_error("undefined token");
                    }
                }
            }
        }
    }
} // half
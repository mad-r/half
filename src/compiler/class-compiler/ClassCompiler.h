//
// Created by a404m on 10/3/23.
//

#ifndef HALF_CLASSCOMPILER_H
#define HALF_CLASSCOMPILER_H

#include <compiler/tree-cleaner/TreeCleaner.h>
#include <compiler/class/Class.h>

namespace half {
    class ClassCompiler {
    public:
        TreeCleaner treeCleaner;

        explicit ClassCompiler(TreeCleaner _treeCleaner);
        explicit ClassCompiler(std::string_view code);
        ClassCompiler(ClassCompiler &&classCompiler) noexcept = default;
        ~ClassCompiler();

    private:
        std::vector<Class*> classes = {
                new Class("nand"),
                new Class("tbuff")
        };

    public:
        Class *getClass(std::string_view className) const;

    private:
        void compileRunNodes();
    };
} // half

#endif //HALF_CLASSCOMPILER_H

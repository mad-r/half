//
// Created by a404m on 9/30/23.
//

#ifndef HALF_PARSER_H
#define HALF_PARSER_H

#include <compiler/lexer/Lexer.h>
#include <map>

namespace half {
    class Parser {
    public:
        Lexer lexer;
        explicit Parser(Lexer _lexer);
        explicit Parser(std::string_view code);

    private:
        static void parseAll(Lexer::Lexed &lexed);

        inline static bool parseHighestOrder(
                Lexer::Lexed::iterator begin,
                Lexer::Lexed::iterator end,
                Lexer::Lexed &lexed
                );
        inline static Lexer::Lexed::iterator findHighestOrder(
                Lexer::Lexed::iterator begin,
                Lexer::Lexed::iterator end
                );

        inline static void parse(
                Lexer::Lexed::iterator it,
                Lexer::Lexed::iterator begin,
                Lexer::Lexed::iterator end,
                Lexer::Lexed &lexed
                );

        inline static Lexer::Lexed::iterator findOpening(
                Lexer::Lexed::iterator it,
                Lexer::Lexed &lexed
                );

        inline static Lexer::Lexed::iterator findLastSemicolon(
                Lexer::Lexed::iterator it,
                Lexer::Lexed &lexed
                );
    };
} // half

#endif //HALF_PARSER_H

//
// Created by a404m on 9/30/23.
//

#include "Parser.h"
#include <algorithm>

namespace half {
    Parser::Parser(Lexer _lexer) : lexer(std::move(_lexer)) {
        parseAll(lexer.lexed);
    }

    Parser::Parser(std::string_view code) : lexer(code) {
        parseAll(lexer.lexed);
    }

    void Parser::parseAll(Lexer::Lexed &lexed) {
        while(parseHighestOrder(lexed.begin(), lexed.end(), lexed));
    }

    bool Parser::parseHighestOrder(
            Lexer::Lexed::iterator begin,
            Lexer::Lexed::iterator end,
            Lexer::Lexed &lexed
            ) {
        auto it = findHighestOrder(begin,end);
        if(it == end || it->getOrder() == -1)
            return false;
        parse(it,begin,end,lexed);
        return true;
    }

    Lexer::Lexed::iterator Parser::findHighestOrder(
            Lexer::Lexed::iterator begin,
            Lexer::Lexed::iterator end
            ) {
        auto result = end;
        int max = -1;
        for(auto it = begin;it < end;++it){
            if(!it->operands.empty())
                continue;
            auto order = it->getOrder();
            if(order > max){
                max = order;
                result = it;
            }
        }
        return result;
    }

    void Parser::parse(
            Lexer::Lexed::iterator it,
            Lexer::Lexed::iterator begin,
            Lexer::Lexed::iterator end,
            Lexer::Lexed &lexed
            ) {
        if(!it->operands.empty())
            throw std::runtime_error("operands is not empty");
        switch (it->getKind()) {
            case Node::Kind::NONE:
                throw std::runtime_error("Kind::NONE");
            case Node::Kind::PREFIX:
                if(it+1 >= end){
                    throw std::logic_error("prefix needs next");
                }
                it->operands.emplace_back(*(it+1));
                lexed.erase(it+1);
                return;
            case Node::Kind::INFIX:
                if(it+1 >= end || it-1 < begin){
                    throw std::logic_error("infix needs both prev and next");
                }
                --it;
                swap(*it,*(it+1));
                it->operands.insert(it->operands.end(),std::make_move_iterator(it+1),std::make_move_iterator(it+3));
                lexed.erase(it+1,it+3);
                return;
            case Node::Kind::POSTFIX:
                throw std::runtime_error("todo");
            case Node::Kind::AROUND: {
                auto close = findOpening(it,lexed);
                if (close >= end || close < begin)
                    throw std::runtime_error("no opening");
                it->operands.insert(it->operands.end(), std::make_move_iterator(close+1), std::make_move_iterator(it));
                it = lexed.erase(close,it);
                parseAll(it->operands);
            }
                return;
            case Node::Kind::END_LINE: {
                const auto lastSemicolon = findLastSemicolon(it,lexed)+1;
                it->operands.insert(it->operands.end(), std::make_move_iterator(lastSemicolon), std::make_move_iterator(it));
                lexed.erase(lastSemicolon, it);
            }
                return;
            case Node::Kind::TWO_NEXT:
                if(it+2 >= end)
                    throw std::runtime_error("");
                it->operands.insert(it->operands.end(),std::make_move_iterator(it+1),std::make_move_iterator(it+3));
                lexed.erase(it+1,it+3);
                return;
            default:
                throw std::runtime_error("class needs a name and a class body");
        }
    }

    Lexer::Lexed::iterator Parser::findOpening(
            Lexer::Lexed::iterator it,
            Lexer::Lexed &lexed
            ) {
        const auto closingToken = it->getOpeningToken();
        --it;
        for(auto begin = lexed.begin();it >= begin;--it){
            if(it->token == closingToken){
                return it;
            }
        }
        return lexed.end();
    }

    Lexer::Lexed::iterator Parser::findLastSemicolon(
            Lexer::Lexed::iterator it,
            Lexer::Lexed &lexed
            ) {
        --it;
        for(auto begin = lexed.begin();it >= begin;--it){
            if(it->token == Node::OP_SEMICOLON){
                return it;
            }
        }
        return lexed.begin()-1;
    }
} // half
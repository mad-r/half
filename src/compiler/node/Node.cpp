//
// Created by a404m on 9/29/23.
//

#include "Node.h"

namespace half {
    Node::Node(Node::Token _token, std::string _str) : token(_token), str(std::move(_str)) {
        //empty
    }

    Node::Node(Node &&node) noexcept: token(node.token), str(std::move(node.str)), operands(std::move(node.operands)) {
        node.token = NONE;
    }

    Node &Node::operator=(Node &&node) noexcept {
        this->token = node.token;
        this->str = std::move(node.str);
        this->operands = std::move(node.operands);
        return *this;
    }

    void Node::clear(Token _token) {
        token = _token;
        str.clear();
        operands.clear();
    }

    void swap(half::Node &n0, half::Node &n1) {
        using std::swap;
        std::swap(n0.token,n1.token);
        std::swap(n0.str,n1.str);
        std::swap(n0.operands,n1.operands);
    }

    Node::Kind Node::getKind() const {
        switch (this->token) {
            case NONE:
            case IDENTIFIER:
            case V_L:
            case V_H:
            case V_Z:
            case V_X:
            case OP_OPEN_CURLY_BRACKET:
                return Kind::NONE;
            case KW_CLASS:
            case KW_GATE:
                return Kind::TWO_NEXT;
//            case KW_IN:
            case KW_INOUT:
//            case KW_OUT:
            case KW_WIRE:
            case KW_RUN:
                return Kind::PREFIX;
            case OP_CLOSE_CURLY_BRACKET:
                return Kind::AROUND;
            case OP_COMMA:
                return Kind::INFIX;
            case OP_SEMICOLON:
                return Kind::END_LINE;
            default:
                throw std::runtime_error("unknown token '"+std::to_string(this->token)+"'");
        }
    }

    int Node::getOrder() const {
        switch (token) {
            case NONE:
            case IDENTIFIER:
                return -1;
            case KW_CLASS:
                return 1;
//            case KW_IN:
            case KW_INOUT:
//            case KW_OUT:
            case KW_WIRE:
            case KW_RUN:
            case KW_GATE:
                return 2;
            case V_L:
            case V_H:
            case V_Z:
            case V_X:
            case OP_OPEN_CURLY_BRACKET:
                return -1;
            case OP_CLOSE_CURLY_BRACKET:
                return 4;
            case OP_COMMA:
                return 3;
            case OP_SEMICOLON:
                return 0;
            default:
                throw std::runtime_error("unknown token '"+std::to_string(this->token)+"'");
        }
    }

    Node::Token Node::getOpeningToken() const {
        switch (this->token) {
            case OP_CLOSE_CURLY_BRACKET:
                return OP_OPEN_CURLY_BRACKET;
            default:
                return NONE;
        }
    }

    bool Node::isRTL() const {
        switch (this->token) {
            case NONE:
            case IDENTIFIER:
            case KW_CLASS:
//            case KW_IN:
            case KW_INOUT:
//            case KW_OUT:
            case KW_WIRE:
            case KW_RUN:
            case KW_GATE:
            case V_L:
            case V_H:
            case V_Z:
            case V_X:
            case OP_CLOSE_CURLY_BRACKET:
            case OP_COMMA:
            case OP_SEMICOLON:
                return false;
            case OP_OPEN_CURLY_BRACKET:
                return true;
            default:
                throw std::runtime_error("unknown token '"+std::to_string(this->token)+"'");
        }
    }
} // half

//
// Created by a404m on 9/29/23.
//

#ifndef HALF_NODE_H
#define HALF_NODE_H

#include <string>
#include <vector>
#include <stdexcept>

namespace half {
    struct Node {
    public:
        friend void swap(half::Node &n0,half::Node &n1);
        enum Token{
            NONE,
            IDENTIFIER,
            KW_CLASS,
            //KW_IN,
            KW_INOUT,
            //KW_OUT,
            KW_WIRE,
            KW_RUN,
            KW_GATE,
            V_L,
            V_H,
            V_Z,
            V_X,
            OP_OPEN_CURLY_BRACKET,
            OP_CLOSE_CURLY_BRACKET,
            OP_COMMA,
            OP_SEMICOLON,
        };

        enum class Kind{
            NONE,
            PREFIX,
            INFIX,
            POSTFIX,
            AROUND,
            END_LINE,
            TWO_NEXT,
        };

        Token token;
        std::string str;
        std::vector<Node> operands;

        explicit Node(Token _token,std::string _str = "");
        Node(const Node &node) = default;
        Node(Node &&node) noexcept;

        Node &operator=(const Node &node) = default;
        Node &operator=(Node &&node) noexcept;

        void clear(Token _token = NONE);

        Kind getKind() const;
        int getOrder() const;

        Token getOpeningToken() const;
        bool isRTL() const;
    };
} // half


#endif //HALF_NODE_H

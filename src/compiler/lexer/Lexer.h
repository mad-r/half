//
// Created by a404m on 9/29/23.
//

#ifndef HALF_LEXER_H
#define HALF_LEXER_H

#include <compiler/node/Node.h>
#include <string_view>
#include <stdexcept>
#include <map>

namespace half {
    class Lexer {
    public:
        using Lexed = std::vector<Node>;
        Lexed lexed;
    public:
        explicit Lexer(std::string_view str);
        Lexer(Lexer &&lexer) noexcept;

    private:
        inline static bool isSpace(char c);
        inline static bool isCharacter(char c);
        inline static bool isNumber(char c);
        inline static bool isValue(char c);
        inline static bool isSymbol(char c);

        inline void pushIfNotNone(Node &node,Node::Token token);
        inline void pushIfNotNoneAnd(Node &node,Node::Token token);

        inline static Node::Token valueToToken(char c);
        inline static Node::Token symbolToToken(char c);
        inline static Node::Token identifierToToken(std::string_view);
    };
} // half

#endif //HALF_LEXER_H

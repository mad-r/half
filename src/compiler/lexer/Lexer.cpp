//
// Created by a404m on 9/29/23.
//

#include "Lexer.h"

namespace half {
    Lexer::Lexer(std::string_view str) {
        Node node{Node::NONE};
        for(auto c : str){
            if(isSpace(c)){
                pushIfNotNone(node,Node::NONE);
            }else if(isCharacter(c) || (node.token == Node::IDENTIFIER && isNumber(c))){
                pushIfNotNoneAnd(node,Node::IDENTIFIER);
                node.str += c;
            }else if(isValue(c)){
                pushIfNotNone(node,valueToToken(c));
            }else if(isSymbol(c)){
                pushIfNotNone(node,symbolToToken(c));
            }else{
                throw std::logic_error(std::string("unknown character '")+c+"'");
            }
        }
        pushIfNotNone(node,Node::NONE);
    }

    Lexer::Lexer(Lexer &&lexer) noexcept : lexed(std::move(lexer.lexed)){
        //empty
    }

    bool Lexer::isSpace(char c) {
        return std::isspace(c);
    }

    bool Lexer::isCharacter(char c) {
        return (c >= 'A' && c <= 'Z')
            || (c >= 'a' && c <= 'z');
    }

    bool Lexer::isNumber(char c) {
        return c >= '0' && c <= '9';
    }

    bool Lexer::isValue(char c) {
        switch (c) {
            case '0'://l
            case '1'://h
            case '!'://z
            case '?'://x
                return true;
            default:
                return false;
        }
    }

    bool Lexer::isSymbol(char c) {
        switch (c) {
            case '{':
            case '}':
            case ',':
            case ';':
                return true;
            default:
                return false;
        }
    }

    void Lexer::pushIfNotNone(Node &node, Node::Token token) {
        if(node.token != Node::NONE){
            if(node.token == Node::IDENTIFIER){
                auto t = identifierToToken(node.str);
                if(t != Node::NONE){
                    node.str.clear();
                    node.token = t;
                }
            }
            swap(node,this->lexed.emplace_back(token));
        }else{
            node.token = token;
        }
    }

    void Lexer::pushIfNotNoneAnd(Node &node,Node::Token token) {
        if(node.token != token){
            pushIfNotNone(node,token);
        }
    }

    Node::Token Lexer::valueToToken(char c) {
        switch (c) {
            case '0':
                return Node::V_L;
            case '1':
                return Node::V_H;
            case '!':
                return Node::V_Z;
            case '?':
                return Node::V_X;
            default:
                return Node::NONE;
        }
    }

    Node::Token Lexer::symbolToToken(char c) {
        switch (c) {
            case '{':
                return Node::OP_OPEN_CURLY_BRACKET;
            case '}':
                return Node::OP_CLOSE_CURLY_BRACKET;
            case ',':
                return Node::OP_COMMA;
            case ';':
                return Node::OP_SEMICOLON;
            default:
                return Node::NONE;
        }
    }

    Node::Token Lexer::identifierToToken(std::string_view str) {
        if(str == "class"){
            return Node::KW_CLASS;
        //}else if(str == "in"){
        //    return Node::KW_IN;
        }else if(str == "inout"){
            return Node::KW_INOUT;
        //}else if(str == "out"){
        //    return Node::KW_OUT;
        }else if(str == "wire"){
            return Node::KW_WIRE;
        }else if(str == "run"){
            return Node::KW_RUN;
        }else if(str == "gate"){
            return Node::KW_GATE;
        }else{
            return Node::NONE;
        }
    }
} // half
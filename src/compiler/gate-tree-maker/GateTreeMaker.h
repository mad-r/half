//
// Created by a404m on 10/3/23.
//

#ifndef HALF_GATETREEMAKER_H
#define HALF_GATETREEMAKER_H

#include <compiler/class-compiler/ClassCompiler.h>
#include <interpreter/gate/Gate.h>

namespace half {
    class GateTreeMaker {
    private:
        ClassCompiler classCompiler;
        Class *main;

    public:
        Gate *root = nullptr;
        std::vector<Wire*> wires;

        explicit GateTreeMaker(ClassCompiler _classCompiler);
        explicit GateTreeMaker(std::string_view code);

        ~GateTreeMaker();

        void makeGateTree();
        void setInouts(const std::vector<Wire::State> &states);
        void run();

        Gate *ejectRootGate();
    };
} // half

#endif //HALF_GATETREEMAKER_H

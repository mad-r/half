//
// Created by a404m on 10/3/23.
//

#include "GateTreeMaker.h"

namespace half {
    GateTreeMaker::GateTreeMaker(ClassCompiler _classCompiler) : classCompiler(std::move(_classCompiler)){
        main = classCompiler.getClass("Main");
        if(main == nullptr){
            throw std::logic_error("no main class found");
        }
        wires.resize(main->inouts.size());
        for(auto &wire : wires){
            wire = new Wire();
        }

        makeGateTree();
    }

    GateTreeMaker::GateTreeMaker(std::string_view code) : classCompiler(code) {
        main = classCompiler.getClass("Main");
        if(main == nullptr){
            throw std::logic_error("no main class found");
        }
        wires.resize(main->inouts.size());
        for(auto &wire : wires){
            wire = new Wire();
        }

        makeGateTree();
    }

    GateTreeMaker::~GateTreeMaker() {
        delete root;
    }

    void GateTreeMaker::makeGateTree() {
        root = new Gate(main,wires,&classCompiler);
        root->argSize = 0;
    }

    void GateTreeMaker::setInouts(const std::vector<Wire::State> &states) {
        for(size_t i = 0;i < wires.size();i++){
            wires[i]->current = states[i];
            wires[i]->next = Wire::State::NONE;
        }
    }

    void GateTreeMaker::run() {
        root->run();
        root->setWiresToNext();
    }

    Gate *GateTreeMaker::ejectRootGate() {
        auto r = root;
        root = nullptr;
        wires.clear();
        return r;
    }
} // half
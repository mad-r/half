//
// Created by a404m on 10/4/23.
//

#include "lib.h"
#include <utils/file-utils/FileUtils.h>

extern half::GateTreeMaker compile(std::string_view code){
    return half::GateTreeMaker(half::ClassCompiler(code));
}

extern half::GateTreeMaker compileFile(std::string_view fileName){
    const auto code = half::FileUtils::readFileAsString(fileName);
    return half::GateTreeMaker(code);
}
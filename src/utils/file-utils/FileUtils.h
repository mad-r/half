//
// Created by a404m on 10/25/23.
//

#ifndef HALF_FILEUTILS_H
#define HALF_FILEUTILS_H

#include <fstream>

namespace half {
    class FileUtils {
    public:
        static std::string readFileAsString(std::string_view path);
    };
} // half

#endif //HALF_FILEUTILS_H

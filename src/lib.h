//
// Created by a404m on 10/4/23.
//

#ifndef HALF_LIB_H
#define HALF_LIB_H

#include <compiler/gate-tree-maker/GateTreeMaker.h>

extern half::GateTreeMaker compile(std::string_view code);

extern half::GateTreeMaker compileFile(std::string_view fileName);

#endif //HALF_LIB_H

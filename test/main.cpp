//
// Created by a404m on 9/29/23.
//

#include <test.h>

bool f(){
    return true;
}

int main(){
    IS_TRUE(f);
    std::cout << std::endl
    << "Tests passed: " << passed << std::endl
    << "Tests failed: " << failed << std::endl;
    return 0;
}
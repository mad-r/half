//
// Created by a404m on 9/30/23.
//

#include <test.h>
#include <compiler/lexer/Lexer.h>

struct LexerTest{
    static void lexedSize(){
        auto lexer = half::Lexer{std::string(R"(
class Main{
    inout a,b,c;
    wire d;
    run{
        gate nand d,a,b;
        gate nand c,a,d;
    }
}
)")};
        IS_TRUE(lexer.lexed.size()==33)
    }

    LexerTest(){
        lexedSize();
    }
};

volatile LexerTest lexerTest{};
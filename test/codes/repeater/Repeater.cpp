//
// Created by a404m on 10/17/23.
//

#include <test.h>
#include <compiler/gate-tree-maker/GateTreeMaker.h>

struct Repeater{
    static void repeater(){
        auto gateTree = half::GateTreeMaker(R"(

class Main{
    inout a;
    run{
        gate Not a,a;
    }
}
class Not{
    inout a,b;
    run{
        gate nand a,b,b;
    }
}
)");
        auto state = half::Wire::State::L;
        gateTree.setInouts({
            state
        });

        for(int i = 0;i < 10;i++){
            if(state == half::Wire::State::L)
                state = half::Wire::State::H;
            else
                state = half::Wire::State::L;
            gateTree.run();

            IS_TRUE(gateTree.wires.front()->current == state);
        }
    }

    Repeater(){
        repeater();
    }
};

volatile auto repeater = Repeater{};
//
// Created by a404m on 9/29/23.
//

#ifndef HALF_TEST_H
#define HALF_TEST_H

#include <iostream>
#include <iomanip>

extern unsigned int passed;
extern unsigned int failed;

#define FILE_NAME_SETW std::setw(20)
#define FUNCTION_NAME_SETW std::setw(20)

#define FG_RED "31"
#define FG_GREEN "32"
#define FG_DEFAULT "39"

#define MAKE_COLORED(color) "\033[" color "m"
#define CLEAR_COLOR "\033[" FG_DEFAULT "m"

#define PASSED std::cout << MAKE_COLORED(FG_GREEN) << "|" << FILE_NAME_SETW << __FILE_NAME__ << "\t" << FUNCTION_NAME_SETW << __FUNCTION__ << "\tpassed line    " << __LINE__ << CLEAR_COLOR << std::endl;++passed;
#define FAILED std::cerr << MAKE_COLORED(FG_RED)   << "*" << FILE_NAME_SETW << __FILE_NAME__ << "\t" << FUNCTION_NAME_SETW << __FUNCTION__ << "\tfailed on line " << __LINE__ << CLEAR_COLOR << std::endl;++failed;

#define IS_TRUE(x) if(x){PASSED}else{FAILED}
#define IS_FALSE(x) if(x){FAILED}else{PASSED}

#define HAS_THROWN(x,throw_type) try{x;FAILED}catch(const throw_type &e){PASSED}

#endif //HALF_TEST_H
